import { CommentReactionModel, CommentModel, UserModel } from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentReactionRepository extends BaseRepository {

  getCommentReaction(userId, commentId) {
    console.log(commentId);
      return this.model.findOne({
          group: [
              'commentReaction.id',
              'comment.id'
          ],
          where: { userId, commentId },
          include: [{
              model: CommentModel,
              attributes: ['id', 'userId']
          }]
      });
  }

    getCommentsReactions( commentId) {
        return this.model.findAll({
            where: {  commentId },
            attributes: ['isLike', 'userId'],
            include: [{
                model: UserModel,
                attributes: ['username']
            }]
        });
    }
}

export default new CommentReactionRepository(CommentReactionModel);
