import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .post('/update', (req, res, next) => commentService.updateComment(req.body) // user added to the request in the jwt strategy, see passport config
        .then((post) => {
            return res.send(post);
        })
    .catch(next))
    .get('/reactions/:id', (req, res, next) => commentService.getCommentsReactions(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(comment => res.send(comment))
        .catch(next))
    .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
            .then(reaction => res.send(reaction))
            .catch(next));

export default router;
