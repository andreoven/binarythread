import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST,
    SET_POST_REACTIONS
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const setPostReactions = postReactions => ({
    type: SET_POST_REACTIONS,
    postReactions
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadPostsExceptMine = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const id = getRootState().profile.user.id;
    const updated = posts.filter(post => post.userId !== id);
    dispatch(setPostsAction(updated));
};

export const loadLikedPosts = filter => async (dispatch, getRootState) => {
    const postReactions  = getRootState().posts.postReactions;
    const { posts: { posts } } = getRootState();
    const id = getRootState().profile.user.id;
    let postsIds = [];
    postReactions.forEach((postReaction) => {
        postReaction.postReactions.forEach((element) => {
            if (element.userId === id && element.isLike === true) {
                postsIds.push(postReaction.id);
            }
        });
    });
    const updated = posts.filter(post => postsIds.includes(post.id) === true);
    dispatch(setPostsAction(updated));
};

export const loadPostReactions = () => async (dispatch) => {
    const postReactions = await postService.getPostReactions();
    dispatch(setPostReactions(postReactions));
};

export const loadCommentReactions = () => async (dispatch) => {
    const commentReactions = await commentService.getCommentReactions();
    dispatch(setPostReactions(commentReactions));
};


export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
    const { id } = await postService.likePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

    const mapLikes = post => ({
        ...post,
        likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
    const { id } = await postService.dislikePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

    const mapLikes = post => ({
        ...post,
        dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
    await commentService.likeComment(commentId);
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
    await commentService.dislikeComment(commentId);
};



export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const deletePost = postId => async (dispatch, getRootState) => {
    const {isDeleted} = await postService.deletePost(postId);

    const mapDelete = post => ({
        ...post,
        isDeleted: true
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapDelete(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapDelete(expandedPost)));
    }

};

export const restorePost = postId => async (dispatch, getRootState) => {
    const {isDeleted} = await postService.restorePost(postId);

    const mapDelete = post => ({
        ...post,
        isDeleted: false
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapDelete(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapDelete(expandedPost)));
    }

};

export const deleteComment = commentId => async (dispatch, getRootState) => {
    const {isDeleted} = await commentService.deleteComment(commentId);
    const commentDelete = comment => ({
        ...comment,
        isDeleted: true
    });

    const { posts: { expandedPost } } = getRootState();
    const updatedComments = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : commentDelete(comment)));
    const updated = {
        ...expandedPost,
        comments: updatedComments
    };
     dispatch(setExpandedPostAction(updated));
};

export const restoreComment = commentId => async (dispatch, getRootState) => {
    const {isDeleted} = await commentService.restoreComment(commentId);
    const commentDelete = comment => ({
        ...comment,
        isDeleted: false
    });
    const { posts: { expandedPost } } = getRootState();
    const updatedComments = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : commentDelete(comment)));
    const updated = {
            ...expandedPost,
            comments: updatedComments
        };
    dispatch(setExpandedPostAction(updated));
};

export const updatePost = (postId, body) => async (dispatch, getRootState) => {
    await postService.updatePost(postId, body);
    const { posts: { posts } } = getRootState();

    const mapUpdate = post => ({
        ...post,
        body: body
    });

    const updated = posts.map(post => (post.id !== postId ? post : mapUpdate(post)));
    dispatch(setPostsAction(updated));
};

export const updateComment = (commentId, body) => async (dispatch, getRootState) => {
    await commentService.updateComment(commentId, body);

    const commentDelete = comment => ({
        ...comment,
        body: body
    });
    const { posts: { expandedPost } } = getRootState();
    const updatedComments = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : commentDelete(comment)));
    const updated = {
            ...expandedPost,
            comments: updatedComments
        };
    dispatch(setExpandedPostAction(updated));
};
