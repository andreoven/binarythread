import React, {useState} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {   updateUser } from 'src/containers/Profile/actions';
import {
    Button,
    Grid,
    Image,
    Input
} from 'semantic-ui-react';


const Profile = ({ user, updateUser }) => {
    const [isEdit, setisEdit] = useState(false);
    const [userInput, setuserInput] = useState('');
    const [emailInput, setemailInput] = useState('');
    const [statusInput, setstatusInput] = useState('');

    (function setInitialState() {
        if (!isEdit) {
            setuserInput(user.username);
            setemailInput(user.email);
            setstatusInput(user.status);
            setisEdit(true);
        }
    }());

    function update() {
        updateUser(user.id, userInput, emailInput, statusInput);
    }

    return (
        <Grid container textAlign="center" style={{ paddingTop: 30 }}>
            <Grid.Column>
                <Image centered src={getUserImgLink(user.image)} size="medium" circular />
                <br />
                <Input
                    icon="user"
                    iconPosition="left"
                    placeholder="Username"
                    type="text"
                    value={userInput}
                    onChange={ev => setuserInput( ev.target.value )}
                />
                <br />
                <br />
                <Input
                    icon="at"
                    iconPosition="left"
                    placeholder="Email"
                    type="email"
                    value={emailInput}
                    onChange={ev => setemailInput( ev.target.value )}
                />
                <br />
                <br />
                <Input
                    icon="info"
                    iconPosition="left"
                    placeholder="Status"
                    type="email"
                    value={statusInput}
                    onChange={ev => setstatusInput( ev.target.value )}
                />
                <br />
                <br />
                <Button type="submit" onClick={() => update()} color="blue" type="submit">Save</Button>
            </Grid.Column>
        </Grid>
        );
};

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any)
};

Profile.defaultProps = {
    user: {}
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user
});

const actions = {
    updateUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps, mapDispatchToProps)(Profile);
