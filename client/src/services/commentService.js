import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/comments',
        type: 'POST',
        request
    });
    return response.json();
};

export const getComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const getCommentReactions = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/reactions/${id}`,
        type: 'GET'
    });
    return response.json();
};


export const likeComment = async (commentId) => {
    const response = await callWebApi({
        endpoint: '/api/comments/react',
        type: 'PUT',
        request: {
            commentId,
            isLike: true
        }
    });
    return response.json();
};

export const dislikeComment = async (commentId) => {
    const response = await callWebApi({
        endpoint: '/api/comments/react',
        type: 'PUT',
        request: {
            commentId,
            isLike: false
        }
    });
    return response.json();
};

export const deleteComment = async (commentId) => {
    const response = await callWebApi({
        endpoint: '/api/comments/update',
        type: 'POST',
        request: {
            commentId,
            isDeleted: true
        }
    });
    return response.json();
};

export const restoreComment = async (commentId) => {
    const response = await callWebApi({
        endpoint: '/api/comments/update',
        type: 'POST',
        request: {
            commentId,
            isDeleted: false
        }
    });
    return response.json();
};

export const updateComment = async (commentId, body) => {
    const response = await callWebApi({
        endpoint: '/api/comments/update',
        type: 'POST',
        request: {
            commentId,
            commentBody: body
        }
    });
    return response.json();
};
