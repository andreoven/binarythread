import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Comment as CommentUI, Form, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import * as commentService from 'src/services/commentService';
import { connect } from 'react-redux';
import { deleteComment, restoreComment, updateComment } from 'src/containers/Thread/actions';
import { bindActionCreators } from 'redux';

import styles from './styles.module.scss';


class Comment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            likes: 0,
            dislikes: 0,
            reactions: [],
            isEdit: false,
            commentBody: ''
        };
    }

    async componentDidMount() {
        const reactions = await commentService.getCommentReactions(this.props.comment.id);
        this.countReactions(reactions);
    }

    async likeComment(id) {
        await this.props.likeComment(id);
        const reactions = await commentService.getCommentReactions(this.props.comment.id);
        this.countReactions(reactions);
    }

    async dislikeComment(id) {
        await this.props.dislikeComment(id);
        const reactions = await commentService.getCommentReactions(this.props.comment.id);
        this.countReactions(reactions);
    }

    getUsersReactedPost(binary) {
        const users = [];
        if (this.state.reactions) {
            this.state.reactions.forEach((element) => {
                if (element.isLike === binary && users.length < 3) {
                    users.push(element.user.username);
                }
            });
            return users.join();
        }
    }

    getDeleteIcon() {
        if (this.props.comment.user.username === this.props.currentUser) {
            return (
                <div>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => this.props.deleteComment(this.props.comment.id)}>
                        <Icon name="times" />
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => { this.setState({ isEdit: true }); this.setState({ commentBody: this.props.comment.body }); }}>
                        <Icon name="edit outline" />
                    </Label>
                </div>
            );
        }
    }

     updateBody = () => {
         this.props.updateComment(this.props.comment.id, this.state.commentBody);
         this.setState({ isEdit: false });
     }

     countReactions(reactions) {
         let commentlikes = 0;
         let commentdislikes = 0;
         reactions.forEach((element) => {
             if (element.isLike) {
                 commentlikes += 1;
             } else {
                 commentdislikes += 1;
             }
         });
         this.setState({
             likes: commentlikes,
             dislikes: commentdislikes,
             reactions
         });
     }

     getDeleteBody(body) {
         if (this.state.isEdit) {
             return (
                 <Form onSubmit={this.updateBody}>
                     <Form.TextArea
                         name="body"
                         value={this.state.commentBody}
                         onChange={ev => this.setState({ commentBody: ev.target.value })}
                     />
                     <Button floated="right" color="blue" type="submit">Save</Button>
                 </Form>
             );
         }
         if (this.props.comment.isDeleted === false) {
             return body;
         }
         return (
             <div>
                 <p>Your comment was deleted</p>
                 <button onClick={() => this.props.restoreComment(this.props.comment.id)} className="ui blue right floated button">Restore comment</button>
             </div>
         );
     }

     render() {
         const { comment: { body, createdAt, user } } = this.props;
         const { comment } = this.props;
         const date = moment(createdAt).fromNow();
         return (
             <CommentUI className={styles.comment}>
                 <CommentUI.Avatar src={getUserImgLink(user.image)} />
                 <CommentUI.Content>
                     <CommentUI.Author as="a">
                         {user.username}
                     </CommentUI.Author>
                     <CommentUI.Metadata>
                         {date}
                     </CommentUI.Metadata>
                     <CommentUI.Text>
                         {this.getDeleteBody(body)}
                     </CommentUI.Text>
                 </CommentUI.Content>
                 <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => this.likeComment(comment.id)}>
                     <Icon name="thumbs up" />
                     {this.state.likes}
                 </Label>
                 <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => this.dislikeComment(comment.id)}>
                     <Icon name="thumbs down" />
                     {this.state.dislikes}
                 </Label>
                 {this.getDeleteIcon(body)}
                 <div className={styles.UsersReactions}>
                     <p>
                            Liked by:
                         {this.getUsersReactedPost(true)}
                     </p>
                     <p>
                            Disliked by:
                         {this.getUsersReactedPost(false)}
                     </p>
                 </div>
             </CommentUI>
         );
     }
}

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired
};

const actions = {
    deleteComment,
    restoreComment,
    updateComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

const mapStateToProps = rootState => ({
    currentUser: rootState.profile.user.username
});

export default connect(mapStateToProps, mapDispatchToProps)(Comment);
