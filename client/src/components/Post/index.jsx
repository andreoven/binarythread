import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {Card, Image, Label, Icon, Form, Button} from 'semantic-ui-react';
import moment from 'moment';


import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost, postReaction, currentUser, deletePost, restorePost, updatePost }) => {
    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt,
        isDeleted
    } = post;

    const [isEdit, setisEdit] = useState(false);
    const [postBody, setpostBody] = useState('');
    const date = moment(createdAt).fromNow();

    function getUsersReactedPost(reaction) {
        let users = [];
        if (postReaction) {
            postReaction.postReactions.forEach((element) => {
                if (element.isLike === reaction && users.length < 3 && element.user.username) {
                    users.push(element.user.username);
                }
            });
            return users.join();
        }
    }

    function getDeleteIcon() {
        if (user.username === currentUser) {
            return (
                <div>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
                        <Icon name="times" />
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => { setisEdit(true); setpostBody(body); }}>
                        <Icon name="edit outline" />
                    </Label>
                </div>
            );
        }
    }

    function updateBody() {
        updatePost(id, postBody);
        setisEdit(false);
    }

    function getDeleteBody() {
        if (isEdit) {
            return (
                <Form onSubmit={updateBody}>
                    <Form.TextArea
                        name="body"
                        value={postBody}
                        onChange={ev => setpostBody( ev.target.value )}
                    />
                    <Button floated="right" color="blue" type="submit">Save</Button>
                </Form>
            )
        } else {
            if (isDeleted === false){
                return body
            } else {
                return (
                    <div>
                        <p>Your post was deleted</p>
                        <button onClick={() => restorePost(id)} className="ui blue right floated button">Restore post</button>
                    </div>
                )
            }
        }
    }

    return (
        <Card style={{ width: '100%' }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by
                        {' '}
                        {user.username}
                        {' - '}
                        {date}
                    </span>
                </Card.Meta>
                <Card.Description>
                    {getDeleteBody()}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
                    <Icon name="thumbs up" />
                    {likeCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
                    <Icon name="thumbs down" />
                    {dislikeCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                    <Icon name="share alternate" />
                </Label>
                {getDeleteIcon()}
            </Card.Content>
            <div className={styles.UsersReactions}>
                <p>Liked by: {getUsersReactedPost(true)}</p>
                <p>Disliked by: {getUsersReactedPost(false)}</p>
            </div>
        </Card>
    );
};


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired
};

export default Post;
